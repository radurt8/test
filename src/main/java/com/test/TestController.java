package com.test;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class TestController {

    @RequestMapping(method = RequestMethod.GET)
    public String helloWorld(Model model) {
        model.addAttribute("notinspiredtocomeupwithagoodvariablename", "It works");
        return "hello";
    }

}
